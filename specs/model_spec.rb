require File.join(File.dirname(__FILE__), 'spec_helper')

describe 'StupidAuth Model' do

  describe 'log in field' do

    it 'defaults to :email' do
      AuthenticatedClass.login_field.should == :email
    end

    it 'allows setting login field to any property of the class' do
      AuthenticatedClass.login_field.should == :email
      AuthenticatedClass.login_with :id
      AuthenticatedClass.login_field.should == :id
    end

    it 'raises an error if an invalid property is assigned' do
      lambda { AuthenticatedClass.login_with :login }.should raise_error('AuthenticatedClass does not respond to :login')
    end

  end

  describe 'password' do

    it 'must be set if there is not an encrypted password' do
      u = AuthenticatedClass.new
      u.should_not be_valid
      u.errors.on(:password).should include("can't be blank")
    end

    it 'must be at least 4 characters long' do
      u = AuthenticatedClass.new :password => 'foo'
      u.should_not be_valid
      u.errors.on(:password).should include('must be at least 4 characters!')

      u = AuthenticatedClass.new :password => 'food'
      u.should_not be_valid
      u.errors.on(:password).should be_blank
    end

  end

  describe 'encrypt_password' do

    it 'hashes a password and sets the password_salt' do
      u = AuthenticatedClass.new
      u.password = 'testing'
      u.encrypted_password.should be_blank

      u.send :encrypt_password

      u.encrypted_password.should_not be_blank
      u.encrypted_password.should_not == 'testing'
      u.password_salt.should_not      be_blank
    end

    it 'is called before valid? if password is set' do
      u = AuthenticatedClass.new :email => 'bob@testing.com', :password => 'password'
      u.encrypted_password.should be_blank
      u.should be_valid
      u.encrypted_password.should_not be_blank
    end

  end

  describe 'authentication' do

    before :all do
      AuthenticatedClass.login_with :email
    end

    # it '#login_field returns the class login field' do
    #   AuthenticatedClass.new.login_field.should == AuthenticatedClass.login_field
    # end
    # 
    # it 'User#login_value returns the value of login field' do
    #   u = AuthenticatedClass.gen
    #   u.login_value.should == u.send(AuthenticatedClass.login_field)
    # end

    it 'returns nil unless a user is found' do
      AuthenticatedClass.authenticate('steve@moo.cow', 'password').should be_nil
    end

    it 'returns nil if a user is found, but cannot be authenticated' do
      AuthenticatedClass.create :email => 'steve@moo.cow', :password => 'testing'
      AuthenticatedClass.authenticate('steve@moo.cow', 'password').should be_nil
    end

    it 'returns the user if a user is found and can be authenticated' do
      user = AuthenticatedClass.create :email => 'steve@moo.cow', :password => 'password'
      AuthenticatedClass.authenticate('steve@moo.cow', 'password').should == user
    end

    it 'is not case sensitive when finding the user' do
      user = AuthenticatedClass.create :email => 'steve@moo.cow', :password => 'password'
      AuthenticatedClass.authenticate('StEvE@mOo.cOw', 'password').should == user
    end

    # this seems a tad redundant
    it 'compares the given password with the encrypted password' do
      u = AuthenticatedClass.create :email => 'steve@moo.cow', :password => 'password'
      u.should_not be_authenticated_by 'foo'
      u.should     be_authenticated_by 'password'
    end

    it 'sets logged_in_at and previous_log_in_at if authenticated' do
      user = AuthenticatedClass.create :email => 'steve@moo.cow', :password => 'password'

      # user has never logged in
      user.reload.logged_in_at.should       be_nil
      user.reload.previous_log_in_at.should be_nil

      # invalid login attempt
      AuthenticatedClass.authenticate 'steve@moo.cow', 'wrong_password'
      user.reload.logged_in_at.should       be_nil
      user.reload.previous_log_in_at.should be_nil

      # first valid login
      AuthenticatedClass.authenticate 'steve@moo.cow', 'password'
      user.reload
      user.logged_in_at.should_not   be_nil
      user.previous_log_in_at.should be_nil

      first_login_time = user.logged_in_at

      # second valid login
      AuthenticatedClass.authenticate 'steve@moo.cow', 'password'
      user.reload
      user.logged_in_at.should_not       be_nil
      user.previous_log_in_at.should_not be_nil

      user.previous_log_in_at.should == first_login_time
    end

  end

  after do
    AuthenticatedClass.destroy_all
  end

end

class CreateAuthenticatedClass < ActiveRecord::Migration

  def self.puts(*args) end # I dont wanna see the migration when the tests run!

  def self.up 
    create_table :authenticated_classes, :force => true do |t|
      t.string   :email
      t.string   :encrypted_password
      t.string   :password_salt
      t.datetime :logged_in_at
      t.datetime :previous_log_in_at
    end
  end

  def self.down
    drop_table :authenticated_classes
  end

end

CreateAuthenticatedClass.migrate :up

class AuthenticatedClass < ActiveRecord::Base

  validates_presence_of   :email, :password_salt, :password
  validates_uniqueness_of :email, :password_salt

  attr_accessor :password

  include StupidAuth::Model

end
