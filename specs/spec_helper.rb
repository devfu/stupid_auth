%w[ rubygems rspec webrat active_record action_controller active_support/core_ext/time/zones ].each { |lib| require lib }

require File.join(File.dirname(__FILE__), '..', 'lib', 'stupid_auth')
require File.join(File.dirname(__FILE__), '..', 'lib', 'stupid_auth', 'webrat_helpers')

ActiveRecord::Base.establish_connection :adapter => 'sqlite3', :database => ':memory:'

include Webrat::Methods
Time.zone = 'UTC'
