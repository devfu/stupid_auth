Gem::Specification.new do |s|
  s.name        = 'stupid_auth'
  s.version     = '0.0.7'
  s.summary     = 'Really basic authentication library.'
  s.description = 'Authenticate users the stupid easy way.'
  s.files       = Dir['lib/**/*.rb']
  s.author      = 'Dev Fu!'
  s.email       = 'info@devfu.com'
  s.homepage    = 'http://github.com/devfu/stupid_auth'
end
