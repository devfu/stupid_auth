Stupid Auth
===========

A very simple user authentication library.

User Model
-----------------
StupidAuth::Model provides some helpful methods for your user model.

<dl>
  <dt>.login_with</dt>
  <dd>sets the field that used to identify the user</dd>

  <dt>.authenticate</dt>
  <dd>find the specified user, and authenticate them</dd>

  <dt>#authenticated_by?</dt>
  <dd>check to see if user is authenticated by a given password</dd>
</dl>


**Application Controller**
----
StupidAuth::Controller provides a few nifty methods for your controllers and views.

<dl>
  <dt>#current_user</dt>
  <dd>returns the user referenced by the session_id</dd>

  <dt>#logged_in?</dt>
  <dd>returns true or false if there a user logged in</dd>

  <dt>#ajax?</dt>
  <dd>returns true if the request was submitted via xhr</dd>

  <dt>#store_location</dt>
  <dd>sets a session variable to the full path of the request</dd>

  <dt>#redirect_back_or_default</dt>
  <dd>redirect back to the stored URL, to the passed in path, or to the root_path</dd>

  <dt>#access_denied</dt>
  <dd>store_location and redirect back to the login_path</dd>

  <dt>#login_required</dt>
  <dd>calls access_denied unless you're a logged in user</dd>

  <dt>#set_thread_user</dt>
  <dd>sets a thread variable for the current user id</dd>
</dl>
