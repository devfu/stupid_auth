module StupidAuth

  module Model

    def self.included base
      class << base
        attr_reader :login_field
      end

      base.extend ClassMethods

      base.send :include,           InstanceMethods
      base.send :validate,          :validate_password
      base.send :before_validation, :encrypt_password
      base.send :login_with,        :email
    end

    module ClassMethods

      # Set the field name to use for authentication
      # Accepts a string or a symbol of a property name
      def login_with field
        field = field.is_a?(Symbol) ? field : field.fieldify.to_sym
        new.respond_to?(field) ? @login_field = field : raise("#{ name } does not respond to #{ field.inspect }")
      end

      # Search the login_field for the given value and
      # attempt to authenticate a found user with the given password
      # set the logged in at timestamp
      def authenticate login, provided_password = nil
        user = where("upper(#{ login_field }) like ?", login.upcase).first
        return nil unless user.present? and user.authenticated_by? provided_password
        user.send :set_logged_in_at!
        user
      end

    end

    module InstanceMethods

      # Compare provided password with the one stored in the database
      def authenticated_by? provided_password
        encrypted_password == Digest::SHA1.hexdigest(provided_password + password_salt)
      end

      # def login_field
      #   self.class.login_field
      # end
      # 
      # def login_value
      #   self.send login_field
      # end

    private

      def set_logged_in_at!
        return nil unless respond_to? :logged_in_at
        update_attribute :previous_log_in_at, self.logged_in_at if respond_to? :previous_log_in_at
        update_attribute :logged_in_at,       Time.zone.now
      end

      # Hash the provided password and persist it, along with a salt
      def encrypt_password
        return true if password.blank?
        self.password_salt      = "stupid-auth-#{ Time.now.to_i }"
        self.encrypted_password = Digest::SHA1.hexdigest(password + password_salt)
        true
      end

      # Ensure that if a password has not been persisted one is provided
      # Ensure that provided passwords are at least 4 characters long
      def validate_password
        if password.present?
          errors.add :password, 'must be at least 4 characters!' unless password.length >= 4
        else
          errors.add :password, "can't be blank" unless encrypted_password.present?
        end

        true
      end

      # create a :reset_password_token that can be used to authenticate
      # a user so that they can reset their passwords. Assumes that there is a reset_password_token
      # attribute on the model.
      def create_reset_password_token!
        update :reset_password_token => Digest::SHA1.hexdigest("password reset token #{ Time.zone.now } #{ rand }")
      end

    end

  end

end