def login_as user
  visit login_path

  fill_in_fields :login => user.send(User.login_field), :password => 'testing'
  click_button 'Login'
end

def logout
  visit logout_path
end
