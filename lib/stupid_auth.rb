# Provides controller and model methods for dealing with user authentication

module StupidAuth
end

require File.join(File.dirname(__FILE__), 'stupid_auth', 'model')
require File.join(File.dirname(__FILE__), 'stupid_auth', 'controller')
